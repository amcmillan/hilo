import mongoose from 'mongoose';

import UserSchema from './schema/user';

let connection = mongoose.createConnection('127.0.0.1', 'hilo', 27017);

connection.model('User', UserSchema);

export default connection;