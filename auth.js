import passport from 'passport';
import {Strategy as LocalStrategy} from 'passport-local';
import bcrypt from 'bcrypt';
import _ from 'lodash';

import db from './db';

passport.use(new LocalStrategy((username, password, done) => {
	console.log(username, password);
	db.model('User').findOne({username}, (err, user) => {
		if (err) return done(err);

		if (!user) {
			return done(null, false, {
				message: 'User not found.'
			});
		}

		bcrypt.compare(password, user.password, (err, result) => {
			if (err) return done(err);

			if (!result) {
				return done(null, false, {
					message: 'Incorrect password.'
				});
			}

			return done(null, user);
		});
	});
}));


passport.serializeUser((user, done) => done(null, user));
passport.deserializeUser((username, done) => db.model('User').findOne({username}, done));

module.exports = passport;