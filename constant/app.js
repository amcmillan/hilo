export default {
	CHANGE: 'CHANGE',
	DUMP_STORE: 'DUMP_STORE',
	ECHO: 'ECHO',
	GUESS: 'GUESS',
	GUESS_RESPONSE: 'GUESS_RESPONSE',
	USER_PICK: 'USER_PICK',
	LOGIN: 'LOGIN',
	CANCEL_LOGIN: 'CANCEL_LOGIN',
	LOGOUT: 'LOGOUT',
	REGISTER: 'REGISTER'
};