import ServerActions from '../action/server';
import $ from 'jquery';

export default {
	/**
	 *
	 */
	get (higher) {
		$.get('/guess/' + higher)
		.done(ServerActions.receiveGuess);
	}
}