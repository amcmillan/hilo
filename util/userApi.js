import ServerActions from '../action/server';
import $ from 'jquery';

export default {
	/**
	 *
	 */
	login (onSuccess, onFail) {
		$.post('/login')
		.done(onSuccess)
		.fail(onFail);
	}
}