import authRouter from './route/auth';
import coreRouter from './route/core';
import guessRouter from './route/guess';
import userRouter from './route/user';

import errorHandler from './route/errorHandler';


export default (app) => {
	app.use('/', coreRouter);

	app.use('/auth', authRouter);
	app.use('/guess', guessRouter);
	app.use('/user', userRouter);

	errorHandler(app);
}