import express from 'express';
import React from 'react';

import db from '../../db';

let router = express.Router();




/**********************************/
/**  CREATE                      **/
/**********************************/

// Create new User
router.post('/', (req, res) => {
	let user = req.body.user;
	let User = db.model('User');

	User.create(user, (err, user) => {
		if (err) {
			let status = err.code === 11000 ? 409 : 500;
			return res.sendStatus(status);
		}

		return res.sendStatus(200);
	});
});





/**********************************/
/**  READ                        **/
/**********************************/

// Get User by username
router.get('/:username', (req, res) => {
	let User = db.model('User');
console.log('get user by username', req.params.username);
	User.findOne({
		username: req.params.username
	}, (err, user) => {
		console.log('get user by username');
		console.log(err, user);
		if (err) return res.sendStatus(500);
		if (!user) return res.sendStatus(404);

		return res.send(user.toClient());
	});
});

export default router;