
import express from 'express';
import passport from '../../auth';
import path from 'path';
import React from 'react';


let router = express.Router();

// User Login
router.post('/login', (req, res, next) => {
	console.log('Auth route Login 1:', req.body.user);
	(passport.authenticate('local', (err, user, info) => {
		console.log('Auth route Login 2: Passport.authenticate cb', err, user, info);
		if (err) return next(err);
		if (!user) return res.sendStatus(403); // Forbidden

		req.logIn(user.username, (err) => {
			console.log('Auth route Login 3: req.logIn cb', err);
			if (err) return next(err);

			res.cookie('user', user.toClient(), {
				maxAge: 1000 * 60 * 60 * 24 * 28,
				httpOnly: false
			});

			return res.sendStatus(200);
		});
	})(req, res, next));
});

// User Logout
router.all('/logout', (req, res) => {
	req.logout();
	req.session.destroy();

	res.clearCookie('user');
	
	return res.sendStatus(200);
});

export default router;