import express from 'express';
import React from 'react';

import ensureAuthentication from '../../middleware/ensureAuthentication';
import db from '../../db';

let router = express.Router();

/**
 *
 */
router.get('/:higher', ensureAuthentication, (req, res) => {
	let number = Math.floor(Math.random() * 100);
	let higher = req.params.higher;
	let last = req.user.guesses[req.user.guesses.length - 1];

	if (!last) {
		last = {
			number: 50
		};
	}

	let correct = higher ? number >= last.number : number <= last.number;

	let guess = {
		_id: db.generateId(),
		correct,
		number,
		timestamp: +new Date()
	};

	res.send(guess);

	db.model('User').findOne(req.user, (err, user) => {
		user.$push({guesses: guess});
		user.$save();
		console.log(user);
	});
});

export default router;