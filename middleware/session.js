import session from 'express-session';
import connectSession from 'connect-mongodb-session';


export default function (app) {
	let MongoDBStore = connectSession(session);

	let sessionStore = new MongoDBStore({ 
		uri: 'mongodb://localhost:27017/hilo',
		collection: 'sessions'
	});

	// Don't emit error 
	sessionStore.on('error', err => {
		assert.ifError(err);
		assert.ok(false);
	});

	app.use(session({
		secret: 'wet cat tuna toast',
		cookie: {
			maxAge: 1000 * 60 * 60 * 24 * 7, // 1 week 
			httpOnly: false
		},
		resave: false,
		saveUninitialized: true,
		store: sessionStore
	}));
}