import bodyParser from 'body-parser';
import session from './session';
import passport from '../auth';


export default (app) => {
	app.use(bodyParser.json());

	session(app);

	app.use(passport.initialize());
	app.use(passport.session());
}