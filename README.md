# react-template #

Boilerplate template for a React project.

## Usage ##
1) Create a new repository.  EG: "react-monkeying-around"

2) Clone this project and change .git to point to the new repository:

```
#!bash

git clone git@bitbucket.org:amcmillan/react-template.git ./react-monkeying-around
cd ./react-monkeying-around
rm -rf .git
git init
git remote add origin git@bitbucket.org:amcmillan/react-monkeying-around.git

```

3) Update project name / url in certain places:

- package.json
- view/index.ejs
- view/error.ejs

4) Add all the files to a new commit and push to the repository:
```
#!bash
git add .
git commit -am "Initial commit"
git push --set-upstream origin master
```

5) Install dependencies

```
#!bash
npm install
```

6) Update this README to reflect your new project!