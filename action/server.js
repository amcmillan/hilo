import AppDispatcher from '../dispatcher/app';
import Constant from '../constant/app';

export default {
	receiveGuess (guess) {
		AppDispatcher.dispatch({
			actionType: Constant.GUESS_RESPONSE,
			guess: guess
		});
	},

	/**
	 *
	 */
	loginSuccess (user) {
		AppDispatcher.dispatch({
			actionType: Constant.LOGIN_SUCCESS,
			user
		});
	},

	/**
	 *
	 */
	loginFail (err) {
		AppDispatcher.dispatch({
			actionType: Constant.LOGIN_FAIL,
			err
		});
	},

	/**
	 *
	 */
	registerSuccess (err, user) {
		AppDispatcher.dispatch({
			actionType: Constant.LOGIN_SUCCESS,
			user
		});

	},

	/**
	 *
	 */
	registerFail (err) {

	}
};