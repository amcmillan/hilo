import AppDispatcher from '../dispatcher/app';
import Constant from '../constant/app';
import GuessApi from '../util/guessApi';
import ServerActions from './server';

export default {
	/**
	 *
	 */
	dumpStore (store) {
		AppDispatcher.dispatch({
			actionType: Constant.DUMP_STORE,
			store
		});
	},

	/**
	 *
	 */
	echo (message) {
		AppDispatcher.dispatch({
			actionType: Constant.ECHO,
			message
		});
	},

	/**
	 *
	 */
	showLoginForm () {
		AppDispatcher.dispatch({
			actionType: Constant.SHOW_LOGIN_FORM
		});
	},

	/**
	 *
	 */
	login (credentials) {
		AppDispatcher.dispatch({
			actionType: Constant.LOGIN
		});

		UserApi.login(ServerActions.loginSuccess, ServerActions.loginFail);
	},

	/**
	 *
	 */
	logout () {
		AppDispatcher.dispatch({
			actionType: Constant.LOGOUT
		});

		UserApi.logout(ServerActions.logoutSuccess);
	},

	/**
	 *
	 */
	cancelLogin () {
		AppDispatcher.dispatch({
			actionType: Constant.CANCEL_LOGIN
		});
	},

	/**
	 *
	 */
	register (credentials) {
		AppDispatcher.dispatch({
			actionType: Constant.REGISTER
		});

		UserApi.register(ServerActions.registerSuccess, ServerActions.registerFail);
	},

	/**
	 *
	 */
	guess (higher) {
		AppDispatcher.dispatch({
			actionType: Constant.GUESS,
			higher
		});

		GuessApi.get(higher);
	}
};