import AppDispatcher from '../dispatcher/app';
import Constant from '../constant/app';
import Store from './store';

// initialise stores
import UserStore from './user';

class AppStore extends Store {
	/**
	 *
	 */
	constructor () {
		super();
		this.data = {};
	}
};

let appStore = new AppStore();

AppDispatcher.register((action) => {
	switch (action.actionType) {
	case Constant.DUMP_STORE:
		if (action.opts.store === 'App') {
			appStore.dumpToConsole();
		}
	break;
	case Constant.ECHO:
		console.log(action.message);
	break;
	}
});

export default appStore;
