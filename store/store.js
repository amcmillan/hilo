import { EventEmitter } from 'events';
import Constant from '../constant/app';

export default class extends EventEmitter {
	/**
	 *
	 */
	constructor () {
		super();
		this.data = {};
	}

	/**
	 *
	 */
	addChangeListener (callback) {
		this.on(Constant.CHANGE, callback);
	}

	/**
	 *
	 */
	removeChangeListener (callback) {
		this.removeListener(Constant.CHANGE, callback);
	}

	/**
	 *
	 */
	getAll () {
		return this.data;
	}

	/**
	 *
	 */
	fetch () {
		$.get({
			url: this.url
		}).done((data) => {
			this.data = data;
			this.emitChange();
		}).fail((err) => {
			console.log('Store Fetch Failure!', err);
		});
	}

	/**
	 *
	 */
	emitChange () {
		this.emit(Constant.CHANGE);
	}

	dumpToConsole () {
		console.log(this.url, this, this.data);
	}
}