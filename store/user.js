import AppDispatcher from '../dispatcher/app';
import Constant from '../constant/app';
import Store from './store';
import $ from 'jquery';

class UserStore extends Store {
	/**
	 *
	 */
	constructor () {
		super();
		this.data = {
			username: 'Guest',
			loggedIn: false,
			showLoginForm: false,
			score: 0,
			guesses: []
		};
	}

	/**
	 *
	 */
	onLoginSuccess (user) {
		this.data.username = user.username;
		this.data.loggedIn = true;
		this.data.showLoginForm = false;
		this.data.score = user.score;
		this.data.guesses = user.guesses;

		this.emitChange();
	}
}

let userStore = new UserStore();

AppDispatcher.register((action) => {
	switch (action.actionType) {
	case Constant.LOGIN_SUCCESS:
		userStore.onLoginSuccess(action.user);
	break;
	case Constant.REGISTER_SUCCESS:
		userStore.onRegisterSuccess
	break;
	case Constant.SHOW_LOGIN_FORM:
		userStore.data.showLoginForm = true;
		userStore.emitChange();
	break;
	case Constant.CANCEL_LOGIN:
		userStore.data.username = '';
		userStore.data.loggedIn = false;
		userStore.data.showLoginForm = false
		userStore.data.score = 0;
		userStore.emitChange();
	break;
	case Constant.LOGOUT:
		userStore.data.username = '';
		userStore.data.loggedIn = false;
		userStore.data.showLoginForm = false;
		userStore.data.score = 0;
		userStore.emitChange()
	break;
	case Constant.GUESS_RESPONSE:
		let guess = action.guess.guess;

		console.log('User store receive guess', guess);

		userStore.data.guesses.push(guess);
		userStore.data.score = guess.correct ? userStore.data.score + 1 : userStore.data.score - 1;
		userStore.emitChange();
	break;
	}
});

export default userStore;
