import React from 'react'
import Component from './component';

import AppActions from '../action/app';
import UserStore from '../store/user';

import Login from './login';

export default class UserInfo extends Component {
	/**
	 *
	 */
	constructor () {
		super();

		this._bind('onChange', 'onLoginClick', 'onLogoutClick');

		this.state = UserStore.getAll();
	}

	/**
	 *
	 */
	componentDidMount () {
		UserStore.addChangeListener(this.onChange);
	}

	/**
	 *
	 */
	componentWillUnmount () {
		UserStore.removeChangeListener(this.onChange);
	}

	/**
	 *
	 */
	render () {
		let content;

		if (this.state.showLoginForm) {
			content = (
				<Login />
			);
		}
		else if (this.state.loggedIn) {
			content = (
				<span key={'username'} className={'username'}>
					{this.state.username + ' '}
					<span className={'logoutButton'} onClick={this.onLogoutClick}>{'(logout)'}</span>
				</span>
			);
		}
		else {
			content = (
				<span key={'username'} className={'username'}>
					{'Guest '}
					<span className={'loginButton'} onClick={this.onLoginClick}>{'(login)'}</span>
				</span>
			);
		}

		return (
			<div className={'userInfo'}>
				{content}
			</div>
		);
	}

	/**
	 *
	 */
	onLoginClick () {
		AppActions.showLoginForm();
	}

	/**
	 *
	 */
	onLogoutClick () {
		AppActions.logout();
	}

	/**
	 *
	 */
	onChange () {
		this.setState(UserStore.getAll());
	}
}