import React from 'react/addons'
import Component from './component';

export default class Game extends Component {
	/**
	 *
	 */
	getClassName () {
		let correct = this.props.correct ? 'correct' : 'incorrect';
		return `number ${correct}`;
	}

	/**
	 *
	 */
	render () {
		return (
			<span className={this.getClassName()}>{this.props.number}</span>
		);
	}
}