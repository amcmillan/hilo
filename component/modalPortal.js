import React from 'react'
import Component from './component';

import $ from 'jquery';


export default class ModalPortal extends Component {
	/**
	 *
	 */
	constructor () {
		super();

		this._bind('onKeyDown', 'onOverlayClick', 'close');
	}

	/**
	 *
	 */
	render () {
		return (
			<div className={'modal modal-overlay'} onClick={this.onOverlayClick} onKeyDown={this.onKeyDown}>
				<div className={'modal modal-content'}>
					{this.props.props.children}
				</div>
			</div>
		);
	}

	/**
	 *
	 */
	componentDidMount () {
		if (this.props.props.onOpen) {
			this.props.props.onOpen();
		}
	}

	/**
	 *
	 */
	onKeyDown (e) {
		if (e.keyCode === 27) {
			e.preventDefault();
			e.stopPropagation();
			this.close();
		}
	}

	/**
	 *
	 */
	onOverlayClick (e) {
		if ($(e.target).closest('.modal-content').length < 1) {
			e.preventDefault();
			e.stopPropagation();

			this.close();
		}
	}

	/**
	 *
	 */
	close () {
		if (this.props.props.onClose) {
			this.props.props.onClose();
		}
	}
}







