import React from 'react'
import Component from './component';
import UserInfo from './userInfo';

import UserStore from '../store/user';

export default class Header extends Component {
	/**
	 *
	 */
	constructor () {
		super();

		this._bind('onUserStoreChange');

		this.state = UserStore.getAll();
	}

	/**
	 *
	 */
	componentDidMount () {
		UserStore.addChangeListener(this.onUserStoreChange);
	}

	/**
	 *
	 */
	componentWillUnmount () {
		UserStore.removeChangeListener(this.onUserStoreChange);
	}

	/**
	 *
	 */
	render () {
		let isNegative = this.state.score < 0 ? 'negative' : '';

		return (
			<header>
				<div className={'left'}>
					<UserInfo />
					<span>{'Score: '}</span>
					<span className={'score ' + isNegative}>{this.state.score}</span>
				</div>
				<div className={'mid'}>
					<h1>{'Hi-Lo!'}</h1>
				</div>
				<div className={'right'}>
				</div>
			</header>
		);
	}

	/**
	 *
	 */
	onUserStoreChange () {
		this.setState(UserStore.getAll());
	}
}
