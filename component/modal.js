import React from 'react'
import Component from './component';

import $ from 'jquery';

import ModalPortal from './modalPortal';


export default class Modal extends Component {
	/**
	 *
	 */
	componentDidMount () {
		this.$el = $('<div class="portal" />');
		$('body').append(this.$el);

		this.renderPortal(this.props);

		$('body').addClass('showingModal');
	}

	/**
	 *
	 */
	componentWillUnmount () {
		React.unmountComponentAtNode(this.$el[0]);

		this.$el.remove()
		delete this.$el;

		$('body').removeClass('showingModal');
	}

	/**
	 *
	 */
	render () {
		return null;	
	}

	/**
	 *
	 */
	renderPortal (props) {
		React.render(<ModalPortal props={props} />, this.$el[0]);
	}
}
