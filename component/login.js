import React from 'react'
import Component from './component';
import Modal from './modal';

import AppActions from '../action/app';

export default class Login extends Component {
	/**
	 *
	 */
	constructor () {
		super();

		this._bind(
			'onKeyUp',
			'onLoginClick',
			'onRegisterClick',
			'onOpenModal',
			'onCloseModal'
		);

		this.state = {
			username: '',
			password: ''
		};
	}

	/**
	 *
	 */
	render () {
		return (
			<Modal onOpen={this.onOpenModal} onClose={this.onCloseModal}>
				<div className={'login'} onKeyUp={this.onKeyUp}>
					<input ref={'username'} type={'text'} placeholder={'Username'} />
					<input ref={'password'} type={'password'} placeholder={'Password'} />
					<button onClick={this.onLoginClick} className={'login'}>{'Login'}</button>
					<button onClick={this.onRegisterClick} className={'register'}>{'Register'}</button>
				</div>
			</Modal>
		);
	}

	/**
	 *
	 */
	onKeyUp (e) {
		if (e.keyCode === 13) {
			e.preventDefault();
			e.stopPropagation();
			return this.onLoginClick();
		}

		this.setState({
			username: React.findDOMNode(this.refs.username).value,
			password: React.findDOMNode(this.refs.password).value
		});
	}

	/**
	 *
	 */
	onLoginClick () {
		AppActions.login(this.state);
	}

	/**
	 *
	 */
	onRegisterClick () {
		AppActions.register(this.state);
	}

	/**
	 *
	 */
	onOpenModal () {
		React.findDOMNode(this.refs.username).focus();
	}

	/**
	 *
	 */
	onCloseModal () {
		AppActions.cancelLogin();
	}
}
