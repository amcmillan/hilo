import React from 'react'

import Component from './component';

import AppActions from '../action/app';
import AppStore from '../store/app';
import UserStore from '../store/user';

import Header from './header';
import Game from './game';


getState () {
	return {
		page: AppStore.getPage(),
		user: UserStore.getAll()
	};
}

export default class App extends Component {
	/**
	 *
	 */
	constructor () {
		super([AppStore, UserStore]);

		this.state = getState();
	}

	/**
	 *
	 */
	render () {
		return (
			<div className={'content'}>
				<Header user={this.state.user} />
				<Game user={this.state.user} />
			</div>
		);
	}
}