import React from 'react/addons'
import Component from './component';
import Number from './number';

import AppActions from '../action/app';
import UserStore from '../store/user';

import Animate from 'rc-animate';
import $ from 'jquery';


let animation = {
	enter: (node, done) => {
		node.style.display = 'none';
		$(node).slideDown('0.5s', () => {
			node.style.display = 'block';
			done.call(null, arguments)
		});
		return {
			stop: () => $(node).stop(true)
		};
	},
	leave: (node, done) => {
		node.style.display = 'block';
		$(node).slideUp('0.5s', done);
		return {
			stop: () => $(node).stop(true)
		};
	}
};

export default class Game extends Component {
	/**
	 *
	 */
	constructor () {
		super();

		this._bind('onUserStoreChange');

		this.state = UserStore.getAll();
	}

	/**
	 *
	 */
	componentDidMount () {
		UserStore.addChangeListener(this.onUserStoreChange);
	}

	/**
	 *
	 */
	componentWillUnmount () {
		UserStore.removeChangeListener(this.onUserStoreChange);
	}

	/**
	 *
	 */
	render () {
		let guessCount = this.state.guesses.length;
		let guesses = this.state.guesses.slice(guessCount - 4);

		if (!guessCount) {
			guesses = [{
				_id: 1,
				number: 50,
				correct: false
			},{
				_id: 2,
				number: 50,
				correct: false
			},{
				_id: 3,
				number: 50,
				correct: false
			},{
				_id: 4,
				number: 50,
				correct: false
			}];
		}

		guesses = guesses.map((guess, i) => {
			return <Number key={guess._id} number={guess.number} correct={guess.correct} index={i} />;
		});

		return (
			<div className={'game'}>
				<img className={'lower'} src={'/img/less-than.png'} onClick={this.onLowerClick}/>
				<div className={'guesses'}>
					<Animate animation={animation}>
						{guesses}
					</Animate>
				</div>
				<img className={'higher'} src={'/img/greater-than.png'} onClick={this.onHigherClick}/>
			</div>
		);
	}

	/**
	 *
	 */
	onLowerClick () {
		AppActions.guess(false);
	}

	/**
	 *
	 */
	onHigherClick () {
		AppActions.guess(true);
	}

	/**
	 *
	 */
	onUserStoreChange () {
		this.setState(UserStore.getAll());
	}
}